<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class komentar extends Model
{
    protected $table = 'komentar';

    protected $fillable = ['user_id','berita_id','isi'];

    public function berita()
    {
        return $this->belongsTo('App\Berita');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
