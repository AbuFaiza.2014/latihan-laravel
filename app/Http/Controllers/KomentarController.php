<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use App\Komentar;

class KomentarController extends Controller
{
    public function store(Request $request){
        $request->validate([
            'isi' => 'required'
        ]);

        $komentar = new Komentar;

        $komentar->isi = $request->isi;
        $komentar->user_id = Auth::id();
        $komentar->berita_id = $request->berita_id;
        

        $komentar->save();

        return redirect()->back();
    }
}
