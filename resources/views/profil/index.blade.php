@extends('layout.master')

@section('judul')
Halaman Update Profil  
@endsection

@section('content')
<form action="/profil/{{$profil->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
        <label>Nama User</label>
        <input type="text" value="{{$profil->user->name}}" class="form-control" disabled>
      </div>
      <div class="form-group">
        <label>Email User</label>
        <input type="text" value="{{$profil->user->email}}" class="form-control" disabled>
      </div>

    <div class="form-group">
      <label>Umur Profil</label>
      <input type="number" name="umur" value="{{$profil->umur}}" class="form-control">
    </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>Biodata</label>
      <textarea name="bio" class="form-control" cols="30" rows="10">{{$profil->bio}}</textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Alamat</label>
        <textarea name="alamat" class="form-control" cols="30" rows="10">{{$profil->alamat}}</textarea>
      </div>
      @error('alamat')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>


@endsection