@extends('layout.master')

@section('judul')
List Berita   
@endsection

@section('content')

@auth
<a href="/berita/create" class="btn btn-primary my-2">Tambah Berita</a>
@endauth



<div class="row">
    @forelse ($berita as $item)
    <div class="col-4">
        <div class="card">
            <img src="{{asset('gambar/'.$item->thumbnail)}}" class="card-img-top" alt="...">
            <div class="card-body">
              <span class="badge badge-info">{{$item->kategori->nama}}</span>
              <h3>{{$item->judul}}</h3>
              <p class="card-text">{{ Str::limit($item->content, 20) }}</p>
              @auth
                <form action="/berita/{{$item->id}}" method="POST">
                  @csrf
                  @method('DELETE')
                  <a href="/berita/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                  <a href="/berita/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                  <input type="submit" value="delete" class="btn btn-danger btn-sm">
                </form>
              @endauth
              
              @guest
              <a href="/berita/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
              @endguest

            </div>
        </div>
    </div>
    @empty
        <h1>Data Berita Masih Kosong</h1>
    @endforelse
</div>


@endsection