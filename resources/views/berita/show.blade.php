@extends('layout.master')

@section('judul')
Halaman Detail Berita {{$berita->id}}
@endsection

@section('content')
<img src="{{asset('gambar/'.$berita->thumbnail)}}" alt="">
<h4>{{$berita->judul}}</h4>
<p>{{$berita->content}}</p>

<h1>Komentar</h1>

@foreach ($berita->komentar as $item)
<div class="card">
  <div class="card-body">
    <small><b>{{$item->user->name}}</b></small>
    <p class="card-text">{{$item->isi}}</p>

  </div>
</div>
    
@endforeach

<form action="/komentar" method="POST" enctype="multipart/form-data" class="my-3">
    @csrf

    <div class="form-group">
        <label>content</label>
        <input type="hidden" name="berita_id" value="{{$berita->id}}" id="">
        <textarea name="isi" class="form-control" cols="30" rows="10"></textarea>
      </div>
      @error('isi')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror

      <button type="submit" class="btn btn-primary">Submit</button>
  </form>

<a href="/berita" class="btn btn-secondary">Kembali</a>

@endsection