@extends('layout.master')

@section('judul')
Halaman Detail Kategori {{$kategori->nama}}   
@endsection

@section('content')

<h3>{{$kategori->nama}}</h3>
<p>{{$kategori->deskripsi}}</p>

<div class="row">
    @foreach ($kategori->berita as $item)
    <div class="col-4">
        <div class="card">
            <img src="{{asset('gambar/'. $item->thumbnail)}}" class="card-img-top" alt="...">
            <div class="card-body">
                <h3>{{$item->judul}}</h3>
                <p class="card-text">{{$item->content}}</p>
            </div>
        </div>
    </div>
    @endforeach
</div>

@endsection