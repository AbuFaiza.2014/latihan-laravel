@extends('layout.master')

@section('judul')
Halaman Tambah Kategori   
@endsection

@section('content')

<form action="/kategori" method="POST">
    @csrf
    <div class="form-group">
      <label>Nama kategori</label>
      <input type="type" name="nama" class="form-control">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
      <label>deskripsi</label>
      <textarea name="deskripsi" class="form-control" cols="30" rows="10"></textarea>
    </div>
    @error('deskripsi')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

@endsection